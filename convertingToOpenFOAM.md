There are multiple ways one can convert the output mesh to OpenFOAM format.

I will try to briefly go over some available options here. 

I will list whether the option involves non-free software. My aim is to be able to convert the mesh using free and open source tools.

However, if one has access to commercial software the whole conversion process becomes much easier. Therefore I think non-free options are worth noting.

# 1. ANSA (non-free)

If you have access to ANSA meshing software, you can simply load the CGNS mesh and export it to OpenFOAM.

You should go into boundary properties (*Ctrl+P*) and define *patch/wall/symmetry/empty* etc 
properties accordingly, as all boundaries will be set to *patch* when importing the CGNS file.

This is all you need to do, ANSA takes care of everything else.

Since the CGNS file contains *FaceCenter* boundary conditions, ANSA automatically translates them to OpenFOAM.

# 2. ICEM CFD (non-free) + fluentMeshToFoam

If you have access to ICEM CFD or ANSYS, you can load the CGNS mesh and export it to ANSYS Fluent format.

I have tested this on ICEM CFD v19.2.

**However, you will need to redefine boundary conditions since ICEM does not read CGNS BCs.**

ICEM CFD will read all CGNS element sections: Volume elements (cells) and boundary elements (faces).

However, no boundary conditions will be assigned to any element section.

You will need to go and assign boundary conditions to the corresponding element sections yourself.

Set output solver to *Ansys Fluent*.

Assign boundary conditions using *Fluent* rules.

Then export the mesh to create the **.msh* file.

To convert, simply copy the resulting **.msh* file into an OpenFOAM directory and use *fluentMeshToFoam* to convert to OpenFOAM format.

Before running the simulation, check boundary properties and run *checkMesh* to ensure that everything regarding the mesh is appropriately defined.

# 3. SALOME (free) + ideasUnvToFoam

If you have acces to SALOME, you can load the CGNS mesh and export it to UNV format.

You can download and use SALOME on any system (it is free and I think open source, though I might be mistaken on the latter).

https://www.salome-platform.org/

Just open SALOME, and go to the *Mesh* module.

Import the CGNS file of your choice and export it to UNV format.

## Something worth noting

When imported in SALOME, the Groups of Faces will inherit the boundary name and the bounary condition from the CGNS file.

Thus you will get names like:

*Airfoil FamilySpecified*

where *Airfoil* is the original boundary name and *FamilySpecified* is the original boundary condition (CGNS files utilize Family boundary conditions).

You can leave names as is, or you can modify them.

In SALOME pipeline browser, you can navigate to *Mesh - Base - Groups of Faces*, and rename everything to your liking.

Then export it to UNV in order to have the names you edited.

Afterwards, simply copy the resulting **.unv* file into an OpenFOAM directory and use *ideasUnvToFoam* to convert to OpenFOAM format.

# 4. plot3dToFoam + autoPatch

If you don't have access to any of the above, or you prefer a command line way to convert the meshes, you can utilize the *plot3dToFoam* toolkit provided with OpenFOAM.

## Initial convert using *plot3dToFoam*

For 3D *plot3d* meshes:

> plot3dToFoam -noBlank *plot3dMeshFilename*

For 2D *plot3d* meshes you should specify that the plot3d mesh is 2D and also provide a thickness:

> plot3dToFoam -2D *thickness* -noBlank -singleBlock *plot3dMeshFilename*

The *singleBlock* entry is required because 2D plot3d output always contains a single block, thus the *blockIndex* integer is omitted from the file.

## Separating boundaries using *autoPatch*

The *plot3dToFoam* toolkit will convert the mesh to OpenFOAM, placing all boundary faces to a single boundary entity called *defaultFaces*. 

This is understandable, since the Plot3d file does not contain boundary conditions.

You will have to utilize the *autoPatch* toolkit, trying different feature angles until you get the boundary patches you want.

*I suggest not overwriting the initial mesh until you get the desired boundary patches. If you overwrite the initial mesh, you will need to reconvert the plot3d file to have the same initial state for autoPatch*.

Thus use:

> autoPatch *angle*

The resulting mesh will be placed in the *1* directory.

In my experience:

> autoPatch 45

tends to work well with most o-type and c-type meshes.

Look at the patches to assess whether they are created correctly. You can visualize them using ParaView.

The automatically created patches will have names like *auto0*/*auto1* etc. You can rename them later on, by modifying the
*constant/polyMesh/boundary* file.

When the patches are created correctly, remove the *1* directory:

> rm -r 1

and run *autoPatch* with overwrite option:

> autoPatch -overwrite 45

This will replace the files in the original *constant/polyMesh/* directory with the ones created by *autoPatch*.

## What to expect from running *autoPatch*

On **2D o-type** meshes you will probably get:
+ an *auto0* boundary which will be the airfoil boundary
+ an *auto1* boundary which will be the low Z-axis side boundary
+ an *auto2* boundary which will be the high Z-axis side boundary
+ an *auto3* boundary which will be the farfield boundary

On **3D o-type** meshes you will probably get:
+ an *auto0* boundary which will be the airfoil boundary
+ an *auto1* boundary which will be the low Z-axis side boundary
+ an *auto2* boundary which will be the farfield boundary
+ an *auto3* boundary which will be the high Z-axis side boundary

On **2D c-type** meshes you will probably get:
+ an *auto0* boundary which will be the low Z-axis side boundary
+ an *auto1* boundary which will be the high Z-axis side boundary
+ an *auto2* boundary which will be the straight (downstream) farfield boundary
+ an *auto3* boundary which will be the airfoil boundary
+ an *auto4* boundary which will be the arced farfield boundary

On **3D c-type** meshes you will probably get:
+ an *auto0* boundary which will be the low Z-axis side boundary
+ an *auto1* boundary which will be the straight (downstream) farfield boundary
+ an *auto2* boundary which will be the airfoil boundary
+ an *auto3* boundary which will be the arced farfield boundary
+ an *auto4* boundary which will be the high Z-axis side boundary

Edit *constant/polyMesh/boundary* with the names you want.

## A final modification that is 100% required

The *autoPatch* utility will leave an empty *defaultFaces* boundary entry in *constant/polyMesh/boundary*.

You have to remove this empty entry and also adjust the total number of boundary objects, typically the number in line 19 of *constant/polyMesh/boundary*, to correctly reflect the new number of boundary objects in the file (essentially subtracting 1 from the original number).

# 5. loading Plot3D or VTK on GMSH + gmshToFoam + autoPatch

This route does not provide any clear advantage over the *plot3dToFoam* route.

On the contrary, boundaries tend to not be correctly assigned.

I suggest using the *plot3dToFoam* utility as described in the previous section.