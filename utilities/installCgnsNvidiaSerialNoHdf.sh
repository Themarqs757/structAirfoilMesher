#####

# if you don't have CGNS downloaded, this script can do it for you
# if you already downloaded the library and you just want to
# compile it, comment out the first section of this script
# that clones the repo and changes dir into it

#
# Git clone CGNS library
#
# v3.4.0-rc1 branch is better for compatibility with
# other programs that utilize CGNS, since most use 3-series CGNS
# 
# In addition, v3.4.0 is compatible with cgns-convert and libcgns-dev that
# are available in Debian/Ubuntu repositories.
git clone -b v3.4.0-rc1 https://github.com/CGNS/CGNS.git
# Can also work with master CGNS branch
## git clone -b master https://github.com/CGNS/CGNS.git

#
# Enter downloaded directory
#
cd CGNS

#####

# Set required environment variables
export CC=mpicc
export FC=mpifort
export CFLAGS=-fpic
export FCFLAGS=-fpic
export LIBS=-ldl
export FLIBS=-ldl

# Enter downloaded directory 
cd src

# clean
make clean

# Set paths
CGNS_TARGET=../buildNvidiaSerialNoHdf

# Configure and make
LIBS="-ldl -lz" ./configure \
--prefix=$CGNS_TARGET \
--enable-64bit \
&& make && make install
