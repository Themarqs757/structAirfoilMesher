!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  author: Konstantinos Diakakis 

subroutine writeGridMapflow(grid, griddim, nplanes, mgrd_lvls, topo, nwake, deltplane, asweep, name)

    Use vardef, only: srf_grid_type

    implicit none

    type(srf_grid_type), intent(in) :: grid
    integer, intent(in) :: griddim, nplanes, mgrd_lvls, nwake
    character(4), intent(in) :: topo
    character(300), intent(in) :: name
    double precision, intent(in) :: deltplane, asweep

    integer imax, jmax, kmax
    integer imax_tot, jmax_tot, kmax_tot
    integer i, j, k, i_reverse
    integer ilo, ihi, jlo, jhi, klo, khi
    integer ilo_airfoil, ihi_airfoil
    integer step
    integer level
    logical threed

    double precision, allocatable :: x_out_3d(:, :, :), y_out_3d(:, :, :), z_out_3d(:, :, :)
    double precision, allocatable :: x_out_2d(:, :), y_out_2d(:, :)
    double precision :: angle, delta_x

    integer, allocatable :: nod(:, :), nel(:, :)

    integer :: nod1, nod2, nod3, nod4
    integer :: nel1, nel2

    integer :: nd, icol, itype, node_nd, nte_type, NFace_type
    integer :: nindx, izoneid, NBC_type
    integer :: nnt_start, nnt_end, nte_start, nte_end
    integer :: nface_start, nface_end
    integer :: nf, n
    integer :: nfaces_xy, nfaces_xz, nfaces_yz
    integer :: nfaces_x, nfaces_y
    integer :: nfaces_ilo, nfaces_ihi, nfaces_jlo, nfaces_jhi, nfaces_wlo, nfaces_whi, nfaces_air, nfaces_klo, nfaces_khi
    integer :: nfaces_tot, nfaces_int
    integer :: wake_faces

    character basename*32, zonename*32
    character filename*64
    character levelstr*6

    threed = .false.
    if (griddim==3 .and. nplanes>1) threed = .true.

    imax_tot = grid%imax
    jmax_tot = grid%jmax
    if (threed) then
        kmax_tot = nplanes
    else
        kmax_tot = 1
    end if

    if (mgrd_lvls.gt.0) then
        if (mod(imax_tot - 1, 2**mgrd_lvls).ne.0) then
            write (*, *) 'Grid imax cannot be divided by multigrid levels.'
            stop
        elseif (mod(jmax_tot - 1, 2**mgrd_lvls).ne.0) then
            write (*, *) 'Grid jmax cannot be divided by multigrid levels.'
            stop
        elseif ((mod(nplanes - 1, 2**mgrd_lvls).ne.0) .and. (threed)) then
            write (*, *) 'Grid kmax cannot be divided by multigrid levels.'
            stop
        end if
    end if

    do level = 0, mgrd_lvls

        if (mgrd_lvls.gt.0) then
            write (*, *) 'Multigrid level: ', level

            write (levelstr, '(a5,i1)') 'level', level
            filename = trim(name)//'_'//trim(levelstr)//'.ascii'
            write (*, *) 'Writing grid to file '//trim(filename)//' ...'
        else
            filename = trim(name)//'.ascii'
            write (*, *) 'Writing grid to file '//trim(filename)//' ...'
        end if

        imax = (imax_tot - 1)/2**level + 1
        jmax = (jmax_tot - 1)/2**level + 1
        if (threed) then
            kmax = (nplanes - 1)/2**level + 1
        else
            kmax = 1
        end if

        if (threed) then
            allocate (x_out_3d(imax, jmax, kmax), y_out_3d(imax, jmax, kmax), z_out_3d(imax, jmax, kmax))
        else
            allocate (x_out_2d(imax, jmax), y_out_2d(imax, jmax))
        end if

        open (1, file=trim(filename))
        step = 2**(level)

        if (threed) then

            !-----
            ! Nodes
            !-----
            nindx = 2
            nd = 3                                      !  Number of dimensions
            write (1, '(i1,1x,i1)') nindx, nd
            nindx = 10                                  !  Index, =10 for nodes, =12 for cells, =13 for faces
            write (1, '(i2)') nindx
            icol = 6                                      !  Number of columns
            izoneid = 0                                 !  Number of zone, defines if the face is interior or boundary
            NNT_start = 1                               !  Starting node
            NNT_end = imax*jmax*kmax                          !  Ending node
            itype = 0
            node_nd = 3
            write (1, 100) icol, nindx, izoneid, NNT_start, NNT_end, itype, node_nd
            izoneid = 8
            itype = 1
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NNT_start, NNT_end, itype, node_nd

            do k = 1, kmax
                if (asweep.gt.0.d0) then
                    angle = asweep*4.d0*atan(1.d0)/180.d0
                    delta_x = tan(angle)*dble((k - 1)*step)*deltplane
                end if
                do j = 1, jmax
                    do i = 1, imax
                        i_reverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        x_out_3d(i, j, k) = grid%x((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                        y_out_3d(i, j, k) = grid%y((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                        z_out_3d(i, j, k) = dble((k - 1)*step)*deltplane
                        if (asweep.gt.0.d0) then
                            x_out_3d(i, j, k) = x_out_3d(i, j, k) + delta_x
                        end if
                        write (1, '(3(f23.16,1x))') x_out_3d(i, j, k), y_out_3d(i, j, k), z_out_3d(i, j, k)
                    end do
                end do
            end do

            !-----
            ! Cells
            !-----
            nindx = 12
            izoneid = 0
            itype = 0
            nte_type = 0
            NTE_start = 1
            NTE_end = (imax - 1)*(jmax - 1)*(kmax - 1)
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NTE_start, NTE_end, itype, nte_type
            izoneid = 9
            itype = 1
            nte_type = 4
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NTE_start, NTE_end, itype, nte_type

            !-----
            ! Faces
            !-----
            nindx = 13
            izoneid = 0
            itype = 0
            NBC_type = 0
            NFace_type = 0
            nte_type = 0

            !-- Calculate number of faces
            !-- Interior faces
            nfaces_xy = (imax - 1)*(jmax - 1)*(kmax - 2)               !  Number of XY faces, Z-sweep
            nfaces_xz = (jmax - 2)*(kmax - 1)*(imax - 1)               !  Number of XZ faces, Y-sweep
            nfaces_yz = (imax - 2)*(kmax - 1)*(jmax - 1)               !  Number of YZ faces, X-sweep

            nfaces_int = nfaces_xy + nfaces_xz + nfaces_yz                   !  Total number of interior faces

            !-- Boundary faces
            nfaces_ilo = (kmax - 1)*(jmax - 1)                      !  Number of faces on ilo
            nfaces_ihi = (kmax - 1)*(jmax - 1)                      !  Number of faces on ihi
            if (topo.eq.'OGRD') then
                nfaces_jlo = (imax - 1)*(kmax - 1)                      !  Number of faces on jlo
            else
                wake_faces = nwake/2**level
                nfaces_wlo = wake_faces*(kmax - 1)                      !  Number of faces on wakelo
                nfaces_air = (imax - 2*wake_faces - 1)*(kmax - 1)           !  Number of faces on airfoil
                nfaces_whi = wake_faces*(kmax - 1)                      !  Number of faces on wakehi
                nfaces_jlo = (imax - 1)*(kmax - 1)
                ilo_airfoil = wake_faces + 1
                ihi_airfoil = imax - wake_faces
            end if
            nfaces_jhi = (imax - 1)*(kmax - 1)                      !  Number of faces on jhi
            nfaces_klo = (imax - 1)*(jmax - 1)                      !  Number of faces on klo
            nfaces_khi = (imax - 1)*(jmax - 1)                      !  Number of faces on khi

            nfaces_tot = nfaces_int + nfaces_ilo + nfaces_ihi + nfaces_jlo + nfaces_jhi + nfaces_klo + nfaces_khi

            NFace_start = 1
            NFace_end = nfaces_tot
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            allocate (nod(nfaces_tot, 4), nel(nfaces_tot, 2))

            !----
            ! interior faces
            !----
            izoneid = 10
            NBC_type = 2
            NFace_type = 4
            NFace_start = 1
            NFace_end = nfaces_int
            write (1, '(i2)') nindx
            write (1, 200) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            !-- XY faces, Z-sweep
            do k = 1, kmax - 2
                do j = 1, jmax - 1
                    do i = 1, imax - 1
                        nf = (k - 1)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i
                        nod(nf, 1) = k*imax*jmax + (j - 1)*imax + i
                        nod(nf, 4) = nod(nf, 1) + 1
                        nod(nf, 3) = k*imax*jmax + j*imax + i + 1
                        nod(nf, 2) = nod(nf, 3) - 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i
                        nel(nf, 2) = k*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i
                        write (1, '(i1,1x,6(i7,1x))') icol, (nod(nf, n), n=1, 4), (nel(nf, n), n=1, 2)
                    end do
                end do
            end do

            !-- XZ faces, Y-sweep
            do j = 1, jmax - 2
                do k = 1, kmax - 1
                    do i = 1, imax - 1
                        nf = nfaces_xy + (j - 1)*(imax - 1)*(kmax - 1) + (k - 1)*(imax - 1) + i
                        nod(nf, 1) = (k - 1)*imax*jmax + j*imax + i
                        nod(nf, 2) = nod(nf, 1) + 1
                        nod(nf, 3) = k*imax*jmax + j*imax + i + 1
                        nod(nf, 4) = nod(nf, 3) - 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i
                        nel(nf, 2) = (k - 1)*(imax - 1)*(jmax - 1) + j*(imax - 1) + i
                        write (1, '(i1,1x,6(i7,1x))') icol, (nod(nf, n), n=1, 4), (nel(nf, n), n=1, 2)
                    end do
                end do
            end do

            !-- YZ faces, X-sweep
            do i = 1, imax - 2
                do k = 1, kmax - 1
                    do j = 1, jmax - 1
                        nf = nfaces_xy + nfaces_xz + (i - 1)*(jmax - 1)*(kmax - 1) + (k - 1)*(jmax - 1) + j
                        nod(nf, 1) = (k - 1)*imax*jmax + (j - 1)*imax + i + 1
                        nod(nf, 4) = (k - 1)*imax*jmax + j*imax + i + 1
                        nod(nf, 3) = k*imax*jmax + j*imax + i + 1
                        nod(nf, 2) = k*imax*jmax + (j - 1)*imax + i + 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i
                        nel(nf, 2) = (k - 1)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i + 1
                        write (1, '(i1,1x,6(i7,1x))') icol, (nod(nf, n), n=1, 4), (nel(nf, n), n=1, 2)
                    end do
                end do
            end do

            !----
            ! Boundary faces
            !----

            ! i=1 boundary
            ! -- wake if o-type
            ! -- farfield if c-type
            izoneid = 1
            if (topo.eq.'OGRD') NBC_type = 20
            if (topo.eq.'CGRD') NBC_type = 10
            NFace_type = 4
            NFace_start = nfaces_int + 1
            NFace_end = NFace_start + nfaces_ilo - 1
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            do k = 1, kmax - 1
                do j = 1, jmax - 1
                    nf = (nface_start - 1) + (k - 1)*(jmax - 1) + j
                    nod(nf, 1) = (k - 1)*imax*jmax + (j - 1)*imax + 1
                    nod(nf, 2) = (k - 1)*imax*jmax + j*imax + 1
                    nod(nf, 3) = k*imax*jmax + j*imax + 1
                    nod(nf, 4) = k*imax*jmax + (j - 1)*imax + 1
                    nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + 1
                    nel2 = 0
                    write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                end do
            end do

            ! i=imax boundary
            ! -- wake if o-type
            ! -- farfield if c-type
            izoneid = 2
            if (topo.eq.'OGRD') NBC_type = 20
            if (topo.eq.'CGRD') NBC_type = 10
            NFace_type = 4
            NFace_start = NFace_end + 1
            NFace_end = NFace_start + nfaces_ihi - 1
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            do k = 1, kmax - 1
                do j = 1, jmax - 1
                    nf = (nface_start - 1) + (k - 1)*(jmax - 1) + j
                    nod(nf, 1) = (k - 1)*imax*jmax + j*imax
                    nod(nf, 4) = (k - 1)*imax*jmax + (j + 1)*imax
                    nod(nf, 3) = k*imax*jmax + (j + 1)*imax
                    nod(nf, 2) = k*imax*jmax + j*imax
                    nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + j*(imax - 1)
                    nel2 = 0
                    write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                end do
            end do

            if (topo.eq.'OGRD') then

                ! j=1 boundary - airfoil
                izoneid = 3
                NBC_type = 3
                NFace_type = 4
                NFace_start = NFace_end + 1
                NFace_end = NFace_start + nfaces_jlo - 1
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                do k = 1, kmax - 1
                    do i = 1, imax - 1
                        nf = (nface_start - 1) + (k - 1)*(imax - 1) + i
                        nod(nf, 1) = (k - 1)*imax*jmax + i
                        nod(nf, 4) = nod(nf, 1) + 1
                        nod(nf, 3) = k*imax*jmax + i + 1
                        nod(nf, 2) = nod(nf, 3) - 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + i
                        nel2 = 0
                        write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                    end do
                end do

            else

                ! j=1 from i=1 to i=wake_faces - wake
                izoneid = 3
                NBC_type = 20
                NFace_type = 4
                NFace_start = NFace_end + 1
                NFace_end = NFace_start + nfaces_wlo - 1
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                do k = 1, kmax - 1
                    do i = 1, ilo_airfoil - 1
                        nf = (nface_start - 1) + (k - 1)*(imax - 1) + i
                        nod(nf, 1) = (k - 1)*imax*jmax + i
                        nod(nf, 4) = nod(nf, 1) + 1
                        nod(nf, 3) = k*imax*jmax + i + 1
                        nod(nf, 2) = nod(nf, 3) - 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + i
                        nel2 = 0
                        write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                    end do
                end do

                ! j=1 from i=wake_faces to imax-wake_faces - airfoil
                izoneid = 3
                NBC_type = 3
                NFace_type = 4
                NFace_start = NFace_end + 1
                NFace_end = NFace_start + nfaces_air - 1
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                do k = 1, kmax - 1
                    do i = ilo_airfoil, ihi_airfoil - 1
                        nf = (nface_start - 1) + (k - 1)*(imax - 1) + i
                        nod(nf, 1) = (k - 1)*imax*jmax + i
                        nod(nf, 4) = nod(nf, 1) + 1
                        nod(nf, 3) = k*imax*jmax + i + 1
                        nod(nf, 2) = nod(nf, 3) - 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + i
                        nel2 = 0
                        write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                    end do
                end do

                ! j=1 from i=imax-wake_faces to imax - wake
                izoneid = 3
                NBC_type = 20
                NFace_type = 4
                NFace_start = NFace_end + 1
                NFace_end = NFace_start + nfaces_whi - 1
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                do k = 1, kmax - 1
                    do i = ihi_airfoil, imax - 1
                        nf = (nface_start - 1) + (k - 1)*(imax - 1) + i
                        nod(nf, 1) = (k - 1)*imax*jmax + i
                        nod(nf, 4) = nod(nf, 1) + 1
                        nod(nf, 3) = k*imax*jmax + i + 1
                        nod(nf, 2) = nod(nf, 3) - 1
                        nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + i
                        nel2 = 0
                        write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                    end do
                end do

            end if

            ! j=jmax boundary
            ! -- always farfield
            izoneid = 6
            NBC_type = 10
            NFace_type = 4
            NFace_start = NFace_end + 1
            NFace_end = NFace_start + nfaces_jhi - 1
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            do k = 1, kmax - 1
                do i = 1, imax - 1
                    nf = (nface_start - 1) + (k - 1)*(imax - 1) + i
                    nod(nf, 1) = (k - 1)*imax*jmax + (jmax - 1)*imax + i
                    nod(nf, 2) = nod(nf, 1) + 1
                    nod(nf, 3) = k*imax*jmax + (jmax - 1)*imax + i + 1
                    nod(nf, 4) = nod(nf, 3) - 1
                    nel(nf, 1) = (k - 1)*(imax - 1)*(jmax - 1) + (jmax - 2)*(imax - 1) + i
                    nel2 = 0
                    write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                end do
            end do

            ! k=1 boundary
            ! -- always side
            izoneid = 7
            NBC_type = 12
            NFace_type = 4
            NFace_start = NFace_end + 1
            NFace_end = NFace_start + nfaces_klo - 1
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            do j = 1, jmax - 1
                do i = 1, imax - 1
                    nf = (nface_start - 1) + (j - 1)*(imax - 1) + i
                    nod(nf, 1) = (j - 1)*imax + i
                    nod(nf, 2) = nod(nf, 1) + 1
                    nod(nf, 3) = j*imax + i + 1
                    nod(nf, 4) = nod(nf, 3) - 1
                    nel(nf, 1) = (j - 1)*(imax - 1) + i
                    nel2 = 0
                    write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                end do
            end do

            ! k=kmax boundary
            ! -- always side
            izoneid = 8
            NBC_type = 12
            NFace_type = 4
            NFace_start = NFace_end + 1
            NFace_end = NFace_start + nfaces_khi - 1
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            do j = 1, jmax - 1
                do i = 1, imax - 1
                    nf = (nface_start - 1) + (j - 1)*(imax - 1) + i
                    nod(nf, 1) = (kmax - 1)*imax*jmax + (j - 1)*imax + i
                    nod(nf, 4) = nod(nf, 1) + 1
                    nod(nf, 3) = (kmax - 1)*imax*jmax + j*imax + i + 1
                    nod(nf, 2) = nod(nf, 3) - 1
                    nel(nf, 1) = (kmax - 2)*(imax - 1)*(jmax - 1) + (j - 1)*(imax - 1) + i
                    nel2 = 0
                    write (1, '(i1,1x,5(i7,1x),i1)') icol, (nod(nf, n), n=1, 4), nel(nf, 1), nel2
                end do
            end do

            deallocate (nod, nel)

        else

            !-----
            ! Nodes
            !-----
            nindx = 2
            nd = 2                                      !  Number of dimensions
            write (1, '(i1,1x,i1)') nindx, nd
            nindx = 10                                  !  Index, =10 for nodes, =12 for cells, =13 for faces
            write (1, '(i2)') nindx
            icol = 6                                      !  Number of columns
            izoneid = 0                                 !  Number of zone, defines if the face is interior or boundary
            NNT_start = 1                               !  Starting node
            NNT_end = imax*jmax                          !  Ending node
            itype = 0
            node_nd = 2
            write (1, 100) icol, nindx, izoneid, NNT_start, NNT_end, itype, node_nd
            izoneid = 8
            itype = 1
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NNT_start, NNT_end, itype, node_nd

            do k = 1, kmax
                do j = 1, jmax
                    do i = 1, imax
                        i_reverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        x_out_2d(i, j) = grid%x((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                        y_out_2d(i, j) = grid%y((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                        write (1, '(2(f23.16,1x))') x_out_2d(i, j), y_out_2d(i, j)
                    end do
                end do
            end do

            !-----
            ! Cells
            !-----
            nindx = 12
            izoneid = 0
            itype = 0
            nte_type = 0
            NTE_start = 1
            NTE_end = (imax - 1)*(jmax - 1)
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NTE_start, NTE_end, itype, nte_type
            izoneid = 9
            itype = 1
            nte_type = 3
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NTE_start, NTE_end, itype, nte_type

            !-----
            ! Faces
            !-----
            nindx = 13
            izoneid = 0
            itype = 0
            NBC_type = 0
            NFace_type = 0
            nte_type = 0

            !-- Calculate number of faces
            !-- Interior faces
            nfaces_y = (imax - 2)*(jmax - 1)  ! y normal faces
            nfaces_x = (imax - 1)*(jmax - 2)  ! x normal faces
            nfaces_int = nfaces_x + nfaces_y

            !-- Boundary faces
            nfaces_ilo = (jmax - 1)
            nfaces_ihi = (jmax - 1)
            if (topo.eq.'OGRD') then
                nfaces_jlo = (imax - 1)
            else
                wake_faces = nwake/2**level
                nfaces_wlo = wake_faces
                nfaces_air = (imax - 2*wake_faces - 1)
                nfaces_whi = wake_faces
                nfaces_jlo = (imax - 1)
                ilo_airfoil = wake_faces + 1
                ihi_airfoil = imax - wake_faces
            end if
            nfaces_jhi = (imax - 1)                      !  Number of faces on jhi

            nfaces_tot = nfaces_int + nfaces_ilo + nfaces_ihi + nfaces_jlo + nfaces_jhi

            NFace_start = 1
            NFace_end = nfaces_tot
            icol = 6                                      !  Number of columns
            write (1, '(i2)') nindx
            write (1, 100) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            !----
            ! interior faces
            !----
            izoneid = 10
            NBC_type = 2
            NFace_type = 2
            NFace_start = 1
            NFace_end = nfaces_int
            icol = 6                                      !  Number of columns
            write (1, '(i2)') nindx
            write (1, 200) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            icol = 4                                      !  Number of columns
            do j = 1, jmax - 2
                do i = 2, imax - 1
                    ! vertical face
                    nod1 = (j - 1)*(imax) + i
                    nod2 = nod1 + imax
                    nel1 = (j - 1)*(imax - 1) + i - 1
                    nel2 = nel1 + 1
                    write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
                    !horizontal face
                    nod1 = nod2
                    nod2 = nod1 - 1
                    nel1 = nel1
                    nel2 = nel1 + (imax - 1)
                    write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
                end do
                i = imax
                !horizontal face
                nod1 = (j)*(imax) + i
                nod2 = nod1 - 1
                nel1 = (j - 1)*(imax - 1) + i - 1
                nel2 = nel1 + (imax - 1)
                write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
            end do
            j = jmax - 1
            do i = 2, imax - 1
                ! vertical face
                nod1 = (j - 1)*(imax) + i
                nod2 = nod1 + imax
                nel1 = (j - 1)*(imax - 1) + i - 1
                nel2 = nel1 + 1
                write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
            end do

            !----
            !  j=jmax boundary - always farfield
            !----
            izoneid = 4
            NBC_type = 10
            NFace_type = 2
            NFace_start = Nface_end + 1
            NFace_end = Nface_start + nfaces_jhi - 1
            icol = 6                                      !  Number of columns
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            icol = 4                                      !  Number of columns
            do i = 1, imax - 1
                nod1 = (jmax - 1)*imax + i + 1
                nod2 = nod1 - 1
                nel1 = (jmax - 2)*(imax - 1) + i
                nel2 = 0
                write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
            end do

            !----
            !  j=1 boundary
            !----
            if (topo.eq.'OGRD') then

                izoneid = 4
                NBC_type = 3
                NFace_type = 2
                NFace_start = Nface_end + 1
                NFace_end = Nface_start + nfaces_jlo - 1
                icol = 6                                      !  Number of columns
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                icol = 4                                      !  Number of columns
                do i = 1, imax - 1
                    nod1 = i
                    nod2 = i + 1
                    nel1 = i
                    nel2 = 0
                    write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
                end do

            else

                izoneid = 4
                NBC_type = 3
                NFace_type = 2
                NFace_start = Nface_end + 1
                NFace_end = Nface_start + nfaces_air - 1
                icol = 6                                      !  Number of columns
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                icol = 4                                      !  Number of columns
                do i = ilo_airfoil, ihi_airfoil - 1
                    nod1 = i
                    nod2 = i + 1
                    nel1 = i
                    nel2 = 0
                    write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
                end do

                izoneid = 4
                NBC_type = 20
                NFace_type = 2
                NFace_start = Nface_end + 1
                NFace_end = Nface_start + nfaces_whi - 1
                icol = 6                                      !  Number of columns
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                icol = 4                                      !  Number of columns
                do i = ihi_airfoil, imax - 1
                    nod1 = i
                    nod2 = i + 1
                    nel1 = i
                    nel2 = 0
                    write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
                end do

                izoneid = 4
                NBC_type = 20
                NFace_type = 2
                NFace_start = Nface_end + 1
                NFace_end = Nface_start + nfaces_wlo - 1
                icol = 6                                      !  Number of columns
                write (1, '(i2)') nindx
                write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

                icol = 4                                      !  Number of columns
                do i = 1, ilo_airfoil - 1
                    nod1 = i
                    nod2 = i + 1
                    nel1 = i
                    nel2 = 0
                    write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
                end do

            end if

            !----
            !  i=imax boundary
            !----
            izoneid = 5
            if (topo.eq.'OGRD') NBC_type = 20
            if (topo.eq.'CGRD') NBC_type = 10
            NFace_type = 2
            NFace_start = Nface_end + 1
            NFace_end = NFace_start + nfaces_ihi - 1
            icol = 6                                      !  Number of columns
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            icol = 4                                      !  Number of columns
            do j = 1, jmax - 1
                nod1 = (j)*imax
                nod2 = (j + 1)*imax
                nel1 = j*(imax - 1)
                nel2 = 0
                write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
            end do

            !----
            !  i=1 boundary
            !----
            izoneid = 4
            if (topo.eq.'OGRD') NBC_type = 20
            if (topo.eq.'CGRD') NBC_type = 10
            NFace_type = 2
            NFace_start = Nface_end + 1
            NFace_end = NFace_start + nfaces_ilo - 1
            icol = 6                                      !  Number of columns
            write (1, '(i2)') nindx
            write (1, 300) icol, nindx, izoneid, NFace_start, NFace_end, NBC_type, NFace_type

            icol = 4                                      !  Number of columns
            do j = 1, jmax - 1
                nod1 = (j)*imax + 1
                nod2 = (j - 1)*imax + 1
                nel1 = (j - 1)*(imax - 1) + 1
                nel2 = 0
                write (1, '(i1,1x,4(i7,1x))') icol, nod1, nod2, nel1, nel2
            end do

        end if

        close (1)

        if (threed) then
            deallocate (x_out_3d, y_out_3d, z_out_3d)
        else
            deallocate (x_out_2d, y_out_2d)
        end if

100     format(i1, 1x, i2, 1x, i1, 1x, i1, 1x, i8, 1x, i1, 1x, i1)
200     format(i1, 1x, i2, 1x, i2, 1x, i1, 1x, i8, 1x, i1, 1x, i1)
300     format(i1, 1x, i2, 1x, i2, 1x, i8, 1x, i8, 1x, i2, 1x, i1)

    end do

end subroutine writeGridMapflow
