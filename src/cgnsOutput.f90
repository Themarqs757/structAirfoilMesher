!  This file is part of structAirfoilMesher.

!  structAirfoilMesher is free software: you can redistribute it and/or modify
!  it under the terms of the GNU General Public License as published by
!  the Free Software Foundation, either version 3 of the License, or
!  (at your option) any later version.

!  structAirfoilMesher is distributed in the hope that it will be useful,
!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
!  GNU General Public License for more details.

!  You should have received a copy of the GNU General Public License
!  along with structAirfoilMesher.  If not, see <http://www.gnu.org/licenses/>.

!  author: Konstantinos Diakakis 
module cgnsOutput

    implicit none

contains

    subroutine writeGridCgns(grid, griddim, nplanes, mgrd_lvls, topo, nwake, deltplane, asweep, name)

        use cgns
        Use vardef, only: srf_grid_type

        type(srf_grid_type), intent(in) :: grid
        integer, intent(in) :: griddim, nplanes, mgrd_lvls, nwake
        character(4), intent(in) :: topo
        character(300), intent(in) :: name
        double precision, intent(in) :: deltplane, asweep

        integer ier

        integer imax, jmax, kmax
        integer imax_tot, jmax_tot, kmax_tot
        integer i, j, k, i_reverse
        integer iLowAirfoil, iHighAirfoil
        integer step
        integer icelldim, iphysdim
        integer level
        integer nodeIndex
        logical threed

        integer(cgsize_t) isize(1, 3)

        !----
        ! CGNS indices
        !----
        integer index_file
        integer index_base, index_zone, index_coord, index_section, index_bc
        integer index_family, index_familybc

        !----
        ! nodes
        !----
        double precision, allocatable :: x_out(:), y_out(:), z_out(:)
        double precision :: angle, delta_x

        !----
        ! elements
        !----
        integer(CGSIZE_T), allocatable :: hexaElem(:, :)
        integer(CGSIZE_T), allocatable :: quadElem(:, :)
        integer(CGSIZE_T), allocatable :: barElem(:, :)
        integer :: elemIndex
        integer :: firstNodeIndex
        integer(CGSIZE_T) :: nElemStart, nElemEnd
        integer :: nBdyElem
        integer :: nQuadsLoc
        integer :: nBarsLoc

        !----
        ! boundary conditions
        !----
        integer(CGSIZE_T), allocatable :: bcPnts(:)
        integer(CGSIZE_T) :: bcCount

        character basename*32, zonename*32
        character bndName*32
        character filename*64
        character levelstr*6

        threed = .false.
        if (griddim==3 .and. nplanes>1) threed = .true.

        imax_tot = grid%imax
        jmax_tot = grid%jmax
        if (threed) then
            kmax_tot = nplanes
        else
            kmax_tot = 1
        end if

        if (mgrd_lvls.gt.0) then
            if (mod(imax_tot - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid imax cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmax_tot - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid jmax cannot be divided by multigrid levels.'
                stop
            elseif ((mod(nplanes - 1, 2**mgrd_lvls).ne.0) .and. (threed)) then
                write (*, *) 'Grid kmax cannot be divided by multigrid levels.'
                stop
            end if
        end if

        do level = 0, mgrd_lvls

            !----
            ! set output filename
            !----
            if (mgrd_lvls.gt.0) then
                write (*, *) 'Multigrid level: ', level
                write (levelstr, '(a5,i1)') 'level', level
                filename = trim(name)//'_'//trim(levelstr)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(name)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if
            !----
            ! define i-j-k bounds
            !----
            imax = (imax_tot - 1)/2**level + 1
            jmax = (jmax_tot - 1)/2**level + 1
            if (threed) then
                kmax = (nplanes - 1)/2**level + 1
            else
                kmax = 1
            end if
            !----
            ! open CGNS file
            !----
            call cg_open_f(trim(filename), CG_MODE_WRITE, index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f
            !----
            ! write CGNS Base node
            !----
            call writeCgnsBase
            !----
            ! write CGNS Zone node
            !----
            call writeCgnsZone
            !----
            ! write node coordinates
            !----
            call writeCgnsNodeCoor
            !----
            ! write volume elements
            !----
            call writeCgnsVolumeElements
            !----
            ! write boundary element sections and boundary conditions
            ! -- based on O-type and C-type, the mesh boundaries have different physical meaning
            !----
            call writeCgnsBoundaryLowI
            call writeCgnsBoundaryHighI
            if (topo.eq.'OGRD') call writeCgnsBoundaryLowJOType
            if (topo.eq.'CGRD') call writeCgnsBoundaryLowJCType
            call writeCgnsBoundaryHighJ
            if (threed) then
                call writeCgnsBoundaryLowK
                call writeCgnsBoundaryHighK
            end if
            !----
            ! close CGNS file
            !----
            call cg_close_f(index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end do

    contains

        Subroutine writeCgnsBase
            !----
            ! base
            !----
            basename = 'Base'
            if (threed) then
                icelldim = 3
                iphysdim = 3
            else
                icelldim = 2
                iphysdim = 2
            end if
            call cg_base_write_f(index_file, basename, icelldim, iphysdim, index_base, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsBase

        subroutine writeCgnsZone

            zonename = 'Zone'

            ! nodes
            isize(1, 1) = imax*jmax*kmax
            ! cells
            if (threed) isize(1, 2) = (imax - 1)*(jmax - 1)*(kmax - 1)
            if (.not. threed) isize(1, 2) = (imax - 1)*(jmax - 1)
            ! boundary
            isize(1, 3) = 0

            call cg_zone_write_f(index_file, index_base, zonename, isize, Unstructured, index_zone, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

        end subroutine writeCgnsZone

        subroutine writeCgnsNodeCoor

            allocate (x_out(imax*jmax*kmax), y_out(imax*jmax*kmax), z_out(imax*jmax*kmax))

            step = 2**(level)

            nodeIndex = 0
            do k = 1, kmax

                if (asweep.gt.0.d0) then
                    angle = asweep*4.d0*atan(1.d0)/180.d0
                    delta_x = dtan(angle)*dble((k - 1)*step)*deltplane
                end if

                do j = 1, jmax
                    do i = 1, imax
                        i_reverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        nodeIndex = nodeIndex + 1
                        if (threed) then
                            x_out(nodeIndex) = grid%x((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out(nodeIndex) = grid%y((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out(nodeIndex) = dble((k - 1)*step)*deltplane
                            if (asweep.gt.0.d0) then
                                x_out(nodeIndex) = x_out(nodeIndex) + delta_x
                            end if
                        else
                            x_out(nodeIndex) = grid%x((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out(nodeIndex) = grid%y((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out(nodeIndex) = 0.d0
                        end if
                    end do
                end do

            end do

            call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out, index_coord, ier)
            call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out, index_coord, ier)
            if (threed) call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateZ', z_out, index_coord, ier)

            deallocate (x_out, y_out, z_out)

        end subroutine writeCgnsNodeCoor

        subroutine writeCgnsVolumeElements

            if (threed) then

                !----
                ! hexas
                !----
                allocate (hexaElem(8, (imax - 1)*(jmax - 1)*(kmax - 1)))

                nElemStart = 1
                elemIndex = 0
                do k = 1, kmax - 1
                    do j = 1, jmax - 1
                        do i = 1, imax - 1

                            elemIndex = elemIndex + 1

                            firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                            hexaElem(1, elemIndex) = firstNodeIndex
                            hexaElem(2, elemIndex) = firstNodeIndex + 1
                            hexaElem(3, elemIndex) = firstNodeIndex + 1 + imax
                            hexaElem(4, elemIndex) = firstNodeIndex + imax
                            hexaElem(5, elemIndex) = firstNodeIndex + imax*jmax
                            hexaElem(6, elemIndex) = firstNodeIndex + imax*jmax + 1
                            hexaElem(7, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                            hexaElem(8, elemIndex) = firstNodeIndex + imax*jmax + imax

                        end do
                    end do
                end do
                nElemEnd = elemIndex
                nBdyElem = 0
                call cg_section_write_f(index_file, index_base, index_zone, 'HexaElements', HEXA_8, nElemStart, nElemEnd, nBdyElem, hexaElem, index_section, ier)

                deallocate (hexaElem)

            else

                !----
                ! hexas
                !----
                allocate (quadElem(4, (imax - 1)*(jmax - 1)))

                nElemStart = 1
                elemIndex = 0
                do j = 1, jmax - 1
                    do i = 1, imax - 1

                        elemIndex = elemIndex + 1

                        firstNodeIndex = i + (j - 1)*imax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + 1
                        quadElem(3, elemIndex) = firstNodeIndex + 1 + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax

                    end do
                end do
                nElemEnd = elemIndex
                nBdyElem = 0
                call cg_section_write_f(index_file, index_base, index_zone, 'QuadElements', QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            end if

        end subroutine writeCgnsVolumeElements

        subroutine writeCgnsBoundaryLowI
            !----
            ! low i boundary
            ! -- WakeLow if O-type
            ! -- CFarfieldLow if C-type
            !----
            if (topo.eq.'OGRD') bndName = 'WakeLow'
            if (topo.eq.'CGRD') bndName = 'CFarfieldLow'

            if (threed) then

                nQuadsLoc = (jmax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = 1
                do k = 1, kmax - 1
                    do j = 1, jmax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = (jmax - 1)
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = 1
                do j = 1, jmax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax
                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowI

        subroutine writeCgnsBoundaryHighI
            !----
            ! ihi boundary
            ! -- WakeHigh if O-type
            ! -- CFarfieldHigh if C-type
            !----
            if (topo.eq.'OGRD') bndName = 'WakeHigh'
            if (topo.eq.'CGRD') bndName = 'CFarfieldHigh'

            if (threed) then

                nQuadsLoc = (jmax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = imax - 1
                do k = 1, kmax - 1
                    do j = 1, jmax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex + 1
                        quadElem(2, elemIndex) = firstNodeIndex + 1 + imax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                        quadElem(4, elemIndex) = firstNodeIndex + imax*jmax + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = (jmax - 1)
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                i = imax - 1
                do j = 1, jmax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax
                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + imax

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, barElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this boundary
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighI

        subroutine writeCgnsBoundaryLowJOType
            !----
            ! jlo boundary
            ! -- Airfoil if O-type
            !----
            bndName = 'Airfoil'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = 1, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJOType

        subroutine writeCgnsBoundaryLowJCType
            !----
            ! jlo boundary
            ! -- the boundary has wake and airfoil regions that
            ! need to be separated
            !----
            iLowAirfoil = nwake/2**level + 1
            iHighAirfoil = imax - nwake/2**level

            ! wakeLow part
            bndName = 'WakeLow'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = 1, iLowAirfoil - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = 1, iLowAirfoil - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            ! airfoil part
            bndName = 'Airfoil'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = iLowAirfoil, iHighAirfoil - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = iLowAirfoil, iHighAirfoil - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

            ! wakeHigh part
            bndName = 'WakeHigh'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do k = 1, kmax - 1
                    do i = iHighAirfoil, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = 1
                do i = iHighAirfoil, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryLowJCType

        subroutine writeCgnsBoundaryHighJ
            !----
            ! high J boundary
            ! -- always farfield
            !----
            bndName = 'Farfield'

            if (threed) then

                nQuadsLoc = (imax - 1)*(kmax - 1)
                allocate (quadElem(4, nQuadsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = jmax
                do k = 1, kmax - 1
                    do i = 1, imax - 1

                        elemIndex = elemIndex + 1
                        firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                        quadElem(1, elemIndex) = firstNodeIndex
                        quadElem(2, elemIndex) = firstNodeIndex + imax*jmax
                        quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1
                        quadElem(4, elemIndex) = firstNodeIndex + 1

                    end do
                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (quadElem)

            else

                nBarsLoc = imax - 1
                allocate (barElem(2, nBarsLoc))

                elemIndex = 0
                nElemStart = nElemEnd + 1

                j = jmax
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax

                    barElem(1, elemIndex) = firstNodeIndex
                    barElem(2, elemIndex) = firstNodeIndex + 1

                end do

                nElemEnd = nElemStart + elemIndex - 1
                nBdyElem = 0

                call cg_section_write_f(index_file, index_base, index_zone, trim(bndName), BAR_2, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

                deallocate (barElem)

            end if

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, trim(bndName), index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, trim(bndName), FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f(trim(bndName), ier)

        end subroutine writeCgnsBoundaryHighJ

        Subroutine writeCgnsBoundaryLowK

            !----
            ! klo boundary
            ! -- SideLow
            !----
            nQuadsLoc = (imax - 1)*(jmax - 1)
            allocate (quadElem(4, nQuadsLoc))

            elemIndex = 0
            nElemStart = nElemEnd + 1

            k = 1
            do j = 1, jmax - 1
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(1, elemIndex) = firstNodeIndex
                    quadElem(2, elemIndex) = firstNodeIndex + 1
                    quadElem(3, elemIndex) = firstNodeIndex + 1 + imax
                    quadElem(4, elemIndex) = firstNodeIndex + imax

                end do
            end do

            nElemEnd = nElemStart + elemIndex - 1
            nBdyElem = 0

            call cg_section_write_f(index_file, index_base, index_zone, 'LateralLow', QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

            deallocate (quadElem)

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, 'LateralLow', index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, 'LateralLow', FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f('LateralLow', ier)

        end subroutine writeCgnsBoundaryLowK

        subroutine writeCgnsBoundaryHighK

            !----
            ! khi boundary
            ! -- SideHigh
            !----
            nQuadsLoc = (imax - 1)*(jmax - 1)
            allocate (quadElem(4, nQuadsLoc))

            elemIndex = 0
            nElemStart = nElemEnd + 1

            k = kmax - 1
            do j = 1, jmax - 1
                do i = 1, imax - 1

                    elemIndex = elemIndex + 1
                    firstNodeIndex = i + (j - 1)*imax + (k - 1)*imax*jmax
                    quadElem(1, elemIndex) = firstNodeIndex + imax*jmax
                    quadElem(2, elemIndex) = firstNodeIndex + imax*jmax + imax
                    quadElem(3, elemIndex) = firstNodeIndex + imax*jmax + 1 + imax
                    quadElem(4, elemIndex) = firstNodeIndex + imax*jmax + 1

                end do
            end do

            nElemEnd = nElemStart + elemIndex - 1
            nBdyElem = 0

            call cg_section_write_f(index_file, index_base, index_zone, 'LateralHigh', QUAD_4, nElemStart, nElemEnd, nBdyElem, quadElem, index_section, ier)

            deallocate (quadElem)

            !----
            ! create family node for this BC
            !----
            call cg_family_write_f(index_file, index_base, 'LateralHigh', index_family, ier)
            call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

            !----
            ! write BC points
            !----
            bcCount = 2
            allocate (bcPnts(bcCount))
            bcPnts(1) = nElemStart
            bcPnts(2) = nElemEnd
            call cg_boco_write_f(index_file, index_base, index_zone, 'LateralHigh', FamilySpecified, PointRange, bcCount, bcPnts, index_bc, ier)
            deallocate (bcPnts)

            call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

            call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
            call cg_famname_write_f('LateralHigh', ier)

        end subroutine writeCgnsBoundaryHighK

    end subroutine writeGridCgns

    subroutine writeGridCgnsStructured(grid, griddim, nplanes, mgrd_lvls, topo, nwake, deltplane, asweep, name)

        use cgns
        Use vardef, only: srf_grid_type

        type(srf_grid_type), intent(in) :: grid
        integer, intent(in) :: griddim, nplanes, mgrd_lvls, nwake
        character(4), intent(in) :: topo
        character(300), intent(in) :: name
        double precision, intent(in) :: deltplane, asweep

        integer imax, jmax, kmax
        integer imax_tot, jmax_tot, kmax_tot
        integer i, j, k, i_reverse
        integer ilo, ihi, jlo, jhi, klo, khi
        integer ilo_airfoil, ihi_airfoil
        integer step
        integer icelldim, iphysdim
        integer level
        logical threed

        double precision, allocatable :: x_out_3d(:, :, :), y_out_3d(:, :, :), z_out_3d(:, :, :)
        double precision, allocatable :: x_out_2d(:, :), y_out_2d(:, :)
        double precision :: angle, delta_x

        integer index_file
        integer index_base, index_zone, index_coord, index_bc
        integer index_family, index_familybc
        integer ier

        integer(cgsize_t) isize_3d(3, 3), ipnts_3d(3, 2)
        integer(cgsize_t) isize_2d(2, 3), ipnts_2d(2, 2)

        character basename*32, zonename*32
        character filename*64
        character levelstr*6

        threed = .false.
        if (griddim==3 .and. nplanes>1) threed = .true.

        imax_tot = grid%imax
        jmax_tot = grid%jmax
        if (threed) then
            kmax_tot = nplanes
        else
            kmax_tot = 1
        end if

        if (mgrd_lvls.gt.0) then
            if (mod(imax_tot - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid imax cannot be divided by multigrid levels.'
                stop
            elseif (mod(jmax_tot - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid jmax cannot be divided by multigrid levels.'
                stop
            elseif (mod(nplanes - 1, 2**mgrd_lvls).ne.0) then
                write (*, *) 'Grid kmax cannot be divided by multigrid levels.'
                stop
            end if
        end if

        do level = 0, mgrd_lvls

            if (mgrd_lvls.gt.0) then
                write (*, *) 'Multigrid level: ', level

                write (levelstr, '(a5,i1)') 'level', level
                filename = trim(name)//'_'//trim(levelstr)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            else
                filename = trim(name)//'.cgns'
                write (*, *) 'Writing grid to file '//trim(filename)//' ...'
            end if

            imax = (imax_tot - 1)/2**level + 1
            jmax = (jmax_tot - 1)/2**level + 1
            if (threed) then
                kmax = (nplanes - 1)/2**level + 1
            else
                kmax = 1
            end if

            call cg_open_f(trim(filename), CG_MODE_WRITE, index_file, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

            !----
            ! base
            !----
            basename = 'Base'
            if (threed) then
                icelldim = 3
                iphysdim = 3
            else
                icelldim = 2
                iphysdim = 2
            end if
            call cg_base_write_f(index_file, basename, icelldim, iphysdim, index_base, ier)
            if (ier.ne.CG_OK) call cg_error_exit_f

            zonename = 'Zone'

            if (threed) then
                ! nodes
                isize_3d(1, 1) = imax
                isize_3d(2, 1) = jmax
                isize_3d(3, 1) = kmax
                ! cells
                isize_3d(1, 2) = imax - 1
                isize_3d(2, 2) = jmax - 1
                isize_3d(3, 2) = kmax - 1
                ! boundary
                isize_3d(1, 3) = 0
                isize_3d(2, 3) = 0
                isize_3d(3, 3) = 0
            else
                ! nodes
                isize_2d(1, 1) = imax
                isize_2d(2, 1) = jmax
                ! cells
                isize_2d(1, 2) = imax - 1
                isize_2d(2, 2) = jmax - 1
                ! boundary
                isize_2d(1, 3) = 0
                isize_2d(2, 3) = 0
            end if

            if (threed) then
                allocate (x_out_3d(imax, jmax, kmax), y_out_3d(imax, jmax, kmax), z_out_3d(imax, jmax, kmax))
            else
                allocate (x_out_2d(imax, jmax), y_out_2d(imax, jmax))
            end if

            step = 2**(level)
            do k = 1, kmax
                if (asweep.gt.0.d0) then
                    angle = asweep*4.d0*atan(1.d0)/180.d0
                    delta_x = dtan(angle)*dble((k - 1)*step)*deltplane
                end if
                do j = 1, jmax
                    do i = 1, imax
                        i_reverse = imax + 1 - i ! x and y are in reverse i order to preserve positive volumes
                        if (threed) then
                            x_out_3d(i, j, k) = grid%x((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out_3d(i, j, k) = grid%y((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            z_out_3d(i, j, k) = dble((k - 1)*step)*deltplane
                            if (asweep.gt.0.d0) then
                                x_out_3d(i, j, k) = x_out_3d(i, j, k) + delta_x
                            end if
                        else
                            x_out_2d(i, j) = grid%x((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                            y_out_2d(i, j) = grid%y((i_reverse - 1)*step + 1, (j - 1)*step + 1)
                        end if
                    end do
                end do
            end do

            if (threed) then

                call cg_zone_write_f(index_file, index_base, zonename, isize_3d, Structured, index_zone, ier)
                if (ier.ne.CG_OK) call cg_error_exit_f

                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out_3d, index_coord, ier)
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out_3d, index_coord, ier)
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateZ', z_out_3d, index_coord, ier)

            else

                call cg_zone_write_f(index_file, index_base, zonename, isize_2d, Structured, index_zone, ier)
                if (ier.ne.CG_OK) call cg_error_exit_f
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateX', x_out_2d, index_coord, ier)
                call cg_coord_write_f(index_file, index_base, index_zone, RealDouble, 'CoordinateY', y_out_2d, index_coord, ier)

            end if

            if (topo.eq.'OGRD') then

                ! boundary patch bounds
                ilo = 1
                ihi = imax
                jlo = 1
                jhi = jmax
                klo = 1
                khi = kmax

                !----
                ! ilo boundary -- Wake
                !----
                if (threed) then

                    !----
                    ! BC point ranges
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ilo
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'WakeLow', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeLow', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('WakeLow', ier)

                else

                    !----
                    ! BC point ranges
                    !----
                    ! lower point of range
                    ipnts_2d(1, 1) = ilo
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ilo
                    ipnts_2d(2, 2) = jhi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'WakeLow', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeLow', BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('WakeLow', ier)

                end if

                !----
                ! ihi boundary -- Wake
                !----
                if (threed) then

                    !----
                    ! BC point ranges
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ihi
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'WakeHigh', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeHigh', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('WakeHigh', ier)

                else

                    !----
                    ! BC point ranges
                    !----
                    ! lower point of range
                    ipnts_2d(1, 1) = ihi
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ihi
                    ipnts_2d(2, 2) = jhi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'WakeHigh', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeHigh', BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('WakeHigh', ier)

                end if

                !----
                ! jlo boundary -- Airfoil
                !----
                if (threed) then

                    !----
                    ! BC point ranges
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jlo
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'Airfoil', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Airfoil', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('Airfoil', ier)

                else

                    !----
                    ! BC point ranges
                    !----
                    ! lower point of range
                    ipnts_2d(1, 1) = ilo
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ihi
                    ipnts_2d(2, 2) = jlo

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'Airfoil', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Airfoil', BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('Airfoil', ier)

                end if

                !----
                ! jhi boundary -- Farfield
                !----
                if (threed) then

                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jhi
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'Farfield', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Farfield', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('Farfield', ier)

                else

                    ! lower point of range
                    ipnts_2d(1, 1) = ilo
                    ipnts_2d(2, 1) = jhi
                    ! higher point of range
                    ipnts_2d(1, 2) = ihi
                    ipnts_2d(2, 2) = jhi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'Farfield', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Farfield', BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('Farfield', ier)

                end if

                if (threed) then

                    !----
                    ! klo boundary -- Side
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = klo

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'SideLow', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'SideLow', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('SideLow', ier)

                    !----
                    ! khi boundary -- Side
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = khi
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'SideHigh', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'SideHigh', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('SideHigh', ier)

                end if

            elseif (topo.eq.'CGRD') then

                ! boundary patch bounds
                ilo = 1
                ihi = imax
                jlo = 1
                jhi = jmax
                klo = 1
                khi = kmax
                ilo_airfoil = nwake/2**level + 1
                ihi_airfoil = imax - nwake/2**level

                !----
                ! ilo boundary -- Farfield
                !----
                if (threed) then

                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ilo
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'Farfield', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Farfield', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('Farfield', ier)

                else

                    ! lower point of range
                    ipnts_2d(1, 1) = ilo
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ilo
                    ipnts_2d(2, 2) = jhi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'Farfield', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Farfield', BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('Farfield', ier)

                end if

                !----
                ! ihi boundary -- Farfield
                !----
                if (threed) then

                    ! lower point of range
                    ipnts_3d(1, 1) = ihi
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'CFarfieldHigh', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'CFarfieldHigh', FamilySpecified, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                    !----
                    ! write BClocation
                    !----
                    call cg_boco_gridlocation_write_f(index_file, index_base, index_zone, index_bc, FaceCenter, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('CFarfieldHigh', ier)

                else

                    ! lower point of range
                    ipnts_2d(1, 1) = ihi
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ihi
                    ipnts_2d(2, 2) = jhi

                    !----
                    ! create family node for this BC
                    !----
                    call cg_family_write_f(index_file, index_base, 'CFarfieldHigh', index_family, ier)
                    call cg_fambc_write_f(index_file, index_base, index_family, 'FamBC1', BCGeneral, index_familybc, ier)

                    !----
                    ! write BC points
                    !----
                    call cg_boco_write_f(index_file, index_base, index_zone, 'CFarfieldHigh', BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)

                    !----
                    ! write nested family node
                    !----
                    call cg_goto_f(index_file, index_base, ier, 'Zone_t', 1, 'ZoneBC_t', 1, 'BC_t', index_bc, 'end')
                    call cg_famname_write_f('CFarfieldHigh', ier)

                end if

                !----
                ! wake 1 - jlo
                !----
                if (threed) then
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ilo_airfoil
                    ipnts_3d(2, 2) = jlo
                    ipnts_3d(3, 2) = khi
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeLow', &
                                         BCGeneral, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                else
                    ! lower point of range
                    ipnts_2d(1, 1) = ilo
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ilo_airfoil
                    ipnts_2d(2, 2) = jlo
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeLow', &
                                         BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                end if

                !----
                ! airfoil - jlo
                !----
                if (threed) then
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo_airfoil
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi_airfoil
                    ipnts_3d(2, 2) = jlo
                    ipnts_3d(3, 2) = khi
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Airfoil', &
                                         BCWallViscous, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                else
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo_airfoil
                    ipnts_3d(2, 1) = jlo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi_airfoil
                    ipnts_3d(2, 2) = jlo
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Airfoil', &
                                         BCWallViscous, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                end if

                !----
                ! wake 2 - jlo
                !----
                if (threed) then
                    ! lower point of range
                    ipnts_3d(1, 1) = ihi_airfoil
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jlo
                    ipnts_3d(3, 2) = khi
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeHigh', &
                                         BCGeneral, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                else
                    ! lower point of range
                    ipnts_2d(1, 1) = ihi_airfoil
                    ipnts_2d(2, 1) = jlo
                    ! higher point of range
                    ipnts_2d(1, 2) = ihi
                    ipnts_2d(2, 2) = jlo
                    call cg_boco_write_f(index_file, index_base, index_zone, 'WakeHigh', &
                                         BCGeneral, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                end if

                !----
                ! Farfield - jhi
                !----
                if (threed) then
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jhi
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Farfield', &
                                         BCFarfield, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                else
                    ! lower point of range
                    ipnts_2d(1, 1) = ilo
                    ipnts_2d(2, 1) = jhi
                    ! higher point of range
                    ipnts_2d(1, 2) = ihi
                    ipnts_2d(2, 2) = jhi
                    call cg_boco_write_f(index_file, index_base, index_zone, 'Farfield', &
                                         BCFarfield, PointRange, 2_cgsize_t, ipnts_2d, index_bc, ier)
                end if

                if (threed) then

                    !----
                    ! klo boundary -- Side
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = klo
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = klo
                    call cg_boco_write_f(index_file, index_base, index_zone, 'SideLow', &
                                         BCWallInviscid, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)
                    !----
                    ! khi boundary -- Side
                    !----
                    ! lower point of range
                    ipnts_3d(1, 1) = ilo
                    ipnts_3d(2, 1) = jlo
                    ipnts_3d(3, 1) = khi
                    ! higher point of range
                    ipnts_3d(1, 2) = ihi
                    ipnts_3d(2, 2) = jhi
                    ipnts_3d(3, 2) = khi
                    call cg_boco_write_f(index_file, index_base, index_zone, 'SideHigh', &
                                         BCWallInviscid, PointRange, 2_cgsize_t, ipnts_3d, index_bc, ier)

                end if

            end if

            call cg_close_f(index_file, ier)

            if (threed) then
                deallocate (x_out_3d, y_out_3d, z_out_3d)
            else
                deallocate (x_out_2d, y_out_2d)
            end if

        end do

    end subroutine writeGridCgnsStructured

end module cgnsOutput
