# Basic steps for contributors

You will have to setup a fork of the repository, make and test your changes, 
push to your origin fork, and then create a pull request for me to pull your changes to the main repo.

The repository (as you can see) is based on GitLab.

I could not find a guide specific to GitLab, however the following link that uses GitHub should help you:

https://www.dataschool.io/how-to-contribute-on-github/

Everything is the same, you will just have to fork and clone a GitLab repository instead of a GitHub repository.

**Always fork the *develop* and not the *master* branch.**

# Coding syntax

The guidelines followed are basic Fortran90/Fortran2003.

Sometimes I like line breaking, sometimes I don't. I just look at the code and decide on the spot if I want
to use linebreaks. If the code is readable and looks ok, I don't mind.

The Makefile has no free format line limit, so if you like long continuous lines you should not have a problem.

**If you create a new file or make changes to an existing file, please add yourself to the authors list that is located at the start of each file.**

I want to make sure each contributor is listed.

# Using *fprettify* for easy code formatting

*fprettify* is a handy python toolkit that automatically handles Fortran formatting.

I suggest you use it, it is pretty simple and works very well!

## Installing *fprettify*

### Debian and derivatives (Ubuntu/Mint/Pop!_OS)

Install fprettify through *pip*:

> sudo apt install pip

> sudo pip install fprettify

or

> pip install fprettify

if you don't want to install it on the *root* partition.

### Arch and derivatives (Manjaro/Endeavour)

I think fprettify is on the AUR.

Use yay to install it

> yay -S fprettify

If it is not in the AUR, just use the *pip* way described above for Debian based systems.

### Fedora/RHEL and derivatives

I have no experience with Fedora based systems, though I think the *pip* way will work.

If you have experience, please help this documentation file by adding instructions!

## *fprettify* arguments that I use

> fprettify -i 4 -l 250 -w 2 --whitespace-relational 0 --enable-decl *fileName*

# Integrating *fprettify* within VSCode

If you use VSCode, you can download the VSCode Modern Fortran formatter extension.

There you can add the *fprettify* binary path and the desired arguments.

Then assign a hotkey to the Format Document command and voila!

You can format each document instantly.