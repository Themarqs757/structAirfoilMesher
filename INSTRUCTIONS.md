# 0. Prerequisites

Building and using *structAirfoilMesher* requires the following:

+ make
+ Fortran compiler
+ compiled CGNS library, if CGNS support is required

If you don't want CGNS, you can compile and use *structAirfoilMesher* without it.

See the *Makefile* for available options.

In general, CGNS support is *opt-in* and not *opt-out*.

Thus, the default builds are without CGNS.

## Linux

### make

*make* is very easy to install in all Linux systems.

For Debian and derivatives like Ubuntu/Mint/Pop!_OS:

> sudo apt install make

For Arch and derivatives:

> sudo pacman -S make

For Fedora/RHEL and derivatives:

> sudo dnf install make

### Fortran compiler

You can use the free and open source GNU compiler called *gfortran*.

For Debian and derivatives:

> sudo apt install gfortran

For Arch and derivatives:

> sudo pacman -S gcc-fortran

For Fedora/RHEL and derivatives:

> sudo dnf install gcc-fortran

Alternatively, you may wany to use Intel or NVIDIA compilers. For installation instructions and environment setup, 
please refer to the corresponding websites.

### CGNS library

Unfortunately, the CGNS library dev packages available in most linux repositories do not always work.

You will need to download and compile the library yourself for full compatibility.

*structAirfoilMesher* only requires basic CGNS features; the library compiling process should not be too hard.

Automated scripts for downloading and building the library using different compilers are provided in the *utilities* folder.

Just make sure your compiler environment is properly set up and then launch the script, for example

> ./install-cgns-gnu-serial-noHdf.sh

for GNU build.

There are also scripts for Intel and NVIDIA compilers.

Compiled library directories have different names based on the compiler, for example

>*downloadedLibraryPath*/buildGnuSerialNoHdf/

for GNU build.

I place all my libraries in

> ~/lib

and that's where the mesher *Makefile.structAirfoilMesher* searches for the corresponding libraries.

If you have already compiled the CGNS library, or you prefer a different install location, 
please adjust the build script and the *Makefile* as needed.

As noted above, *structAirfoilMesher* builds with different compilers search in different paths for the
corresponding CGNS library.

Make sure that the CGNS library has already been built when trying a specific (GNU/Intel/NVIDIA) build with CGNS
support.

## Windows

### WSL

If you use WSL, you can simply follow the directions in the previous paragraph that 
correspond to your specific distribution.

You will most likely have Ubuntu or Debian, since that's the most popular options available
in the Microsoft Store.

### mingW

*minGW* will probably be ok in regards to the compiler, though I have to test if everything works.

I think *gfortran* and *make* are bound to work ok, although I don't know about the CGNS library.

### Visual Studio

Under construction.

### Cmake

Under construction.

# 1. Compiling structAirfoilMesher

## a. Load your prefered compiler environment

GNU compilers are already on *PATH*, so you don't need to do anything.

For Intel/NVIDIA, you have to set the appropriate environment variables.

Follow the instructions on the corresponding websites.

## b. Check the *Makefile* to get an overview of your build options.

Use the text editor of your choice to view *Makefile* contents.

You will see that there are different build options for different compilers and configurations.

For example

> make intel

for Intel build without CGNS support,

> make nvidiaCgns

for NVIDIA build with CGNS support,

> make gnuCgns

for GNU build with CGNS support.

Debug configurations are also available, for example

> make intelDebug

for debug Intel build without CGNS support,

> make nvidiaCgnsDebug

for debug NVIDIA build with CGNS support.

## c. Run the build command of your choice

> make gnuCgns

## d. Binary location

Created binaries will be placed in x64/Release or x64/Debug, depending on Release/Debug build.


# 2. Creating a mesh 

Different geometries and sample input files are provided for testing purposes.

You can use the utility in both interactive and scripted modes.

To run *structAirfoilMesher* in interactive mode just launch the executable:

> *pathToBinary*/structAirfoilMesher

This will initiate the interactive command prompt. You will see instructions and options for everything.

If you prefer to have a predefined parameter file (useful for batch mesh creation) you can use the following syntax:

> *pathToBinary*/structAirfoilMesher *geometryFilename* < *inputParameterFilename*

You can also place *geometryFilename* in the first line of *inputParameterFilename*.

The syntax then becomes:

> *pathToBinary*/structAirfoilMesher < *inputParameterFilename*

# 3. Visualizing output files

Output mesh files can be viewed with visualization software like ParaView, gmsh, SALOME, or Tecplot.

## Visualization incompatibilities

1. ParaView cannot open plot3d files. This has nothing to do with structAirfoilMesher, grids created by NASA also do not work.

2. GMSH cannot open 2D plot3d files. 3D plot3d files work as intended.

3. GMSH from Debian/Ubuntu repositories cannot open CGNS files. 
This has also nothing to do with structAirfoilMesher, grids created by NASA also do not work. 
If you are on Arch/Manjaro, GMSH from the AUR has better CGNS support and works as intended. 
For Debian/Ubuntu, you can try downloading the GMSH binaries from https://gmsh.info/#Download, which can handle CGNS files for visualization purposes.

# 4. Converting meshes to OpenFOAM

Please refer to *convertingToOpenFOAM.md*.

# 5. Converting meshes to Fluent

Please refer to *convertingToFluent.md*.

# 6. Known issues

## GNU build on Debian/Ubuntu based systems suddenly exits when entering the *EXIT* command in a submenu

### **UPDATE** Bug fixed by Samir Ouchene <samir.ouchene@g.enp.edu.dz>. See commit d9b2250522c5c85c7d38868045240ed3b0318cb2 for the specific commit and https://unix.stackexchange.com/a/610101/267622 for the bug description!

This weird behavior only appears in Debian/Ubuntu based systems and only when built with GNU compiler.

When entering the *EXIT* command in order to exit one of the program's submenus, the program simply exits completely.

I don't know why this occurs, though as mentioned it only appears on GNU builds in Debian/Ubuntu based systems.

The weird thing is that it sometimes it works and sometimes it doesn't.

It never appears when using Intel/Nvidia builds.

It also does not appear in GNU builds in Arch based systems, probably because they have newer *gfortran* versions.

I have no experience on Fedora.

## Plot3d boundary conditions file (*.nmf*) is only written for the finest (initial) mesh in case of multigrid output

The **.nmf* file is not used that often, thus I didn't find it necessary to add multigrid support for it.

If you want to use it with multigrid meshes, you can simply edit the corresponding lines with the new grid dimensions.

If users request it, maybe I can add proper **.nmf* output in the future.
